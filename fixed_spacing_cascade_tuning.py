import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
import util.skrf_media_tools
import util.cascading_tools
import math
from scipy.optimize import minimize, rosen, rosen_der


def main():

    to_port = 2
    from_port = 1
    directory = r"data"
    file_name = r"50Ohm_unit_cell.s2p"
    cpw_data_file_path = os.path.join(directory, file_name)

    parasitic_spacing = (
        500e-6  # Total amount of transmission line between parasitic capacitance.
    )
    total_parasitic_line_length = 27e-3  # Total length of transmission line.
    parasitic_capacitance = 1.792569790752858e-14

    frequency_array = np.linspace(0.5e9, 3.5e9, num=1001)
    # Define frequency band:
    frequency_band = rf.Frequency(
        start=frequency_array[0],
        stop=frequency_array[-1],
        unit="Hz",
        npoints=frequency_array.size,
    )

    # Create rf.Media instance from cpw simulation data.
    cpw_media = util.skrf_media_tools.create_media(
        frequency_band=frequency_band,
        media_simulation_file_path=cpw_data_file_path,
        loss_tangent=0.0,
    )

    gamma_array = util.skrf_media_tools.calculate_gamma(
        frequency_band=frequency_band,
        effective_permittivity=complex(6.41536, 0.0),
        loss_tangent=0.0,
    )

    # Length remaining after fitting as many parasitic unit cells in:
    remaining_length = (total_parasitic_line_length - 200e-6) % parasitic_spacing
    number_of_parasitics = math.floor((total_parasitic_line_length - 200e-6) / parasitic_spacing)

    fixed_spacing_parasitic_cascade_network = util.cascading_tools.build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance=parasitic_capacitance,
        fixed_parasitic_spacing=parasitic_spacing,
        number_of_parasitics=number_of_parasitics,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=remaining_length,
        output_line_length=remaining_length,
    )

    tuned_z0_array = [46.5, 50]

    plt.figure(figsize=(8, 6))
    for tuned_z0 in tuned_z0_array:
        tuning_cpw_media = rf.media.DefinedGammaZ0(
            frequency=frequency_band,
            z0=complex(tuned_z0, 0),
            gamma=gamma_array,
        )

        tuning_cpw_line = tuning_cpw_media.line(10e-3, units="m")

        full_network = (
            tuning_cpw_line
            ** fixed_spacing_parasitic_cascade_network
            ** tuning_cpw_line
            ** fixed_spacing_parasitic_cascade_network
            ** tuning_cpw_line
        )
        plt.plot(
            full_network.f * 1e-9,
            full_network.s_db[:, from_port - 1, to_port - 1],
            linestyle="-",
            label=f"{tuned_z0} $\Omega$"
        )
    plt.legend()
    plt.title(
        "Feedline throughput for a variable z0 simple CPW connecting two fixed\nspacing parasitic cascades"
    )
    plt.legend(title="Tuning CPW Z0:")
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S%i%i (dB)" % (to_port, from_port))
    plt.show()


if __name__ == "__main__":
    main()
