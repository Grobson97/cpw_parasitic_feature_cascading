import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import util.skrf_media_tools


def main():

    to_port = 2
    from_port = 1
    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\FBS_LEKIDs\hybrid_idc_lekid\300um_SiN_Qc_study"
    file_name = "hybrid_idc_lekid_auto_lengths_lossless_test_param1.s2p"

    file_path = os.path.join(directory, file_name)

    # Create renormalised port impedance network from file:
    lekid_network = util.sonnet_simulation_tools.make_renormalised_network(
        data_file_path=file_path, n_ports=2
    )

    fit_params = util.lekid_analysis_tools.fit_skewed_lorentzian(
        frequency_array=lekid_network.f,
        data_array=np.abs(lekid_network.s[:, from_port - 1, to_port - 1]),
        qc_guess=5e5,
        qi_guess=1e8,
        f0_guess=None,
        normalise=False,
        plot_graph=False,
        plot_db=True,
        plot_title="Isolated LEKID network",
    )

    z0_array = [5, 10, 20, 30, 40, 50, 60]
    tuning_line_length = 20e-3
    gamma_array = util.skrf_media_tools.calculate_gamma(
        frequency_band=lekid_network.frequency,
        effective_permittivity=complex(6.41536, 0.0),
        loss_tangent=0.0,
    )

    qc_array = []
    qi_array = []
    f0_array = []
    s_db_array = []

    for z0 in z0_array:

        tuning_cpw_media = rf.media.DefinedGammaZ0(
            frequency=lekid_network.frequency,
            z0=complex(z0, 0),
            gamma=gamma_array,
        )

        tuning_cpw_line = tuning_cpw_media.line(tuning_line_length, units="m")
        tuned_lekid_network = tuning_cpw_line ** lekid_network ** tuning_cpw_line

        fit_params = util.lekid_analysis_tools.fit_skewed_lorentzian(
            frequency_array=tuned_lekid_network.f,
            data_array=np.abs(tuned_lekid_network.s[:, from_port - 1, to_port - 1]),
            qc_guess=5e5,
            qi_guess=1e8,
            f0_guess=None,
            normalise=False,
            plot_graph=False,
            plot_db=True,
            plot_title=f"LEKID network within {z0}$\Omega$ transmission line",
        )

        qc_array.append(fit_params["qc"][0])
        qi_array.append(fit_params["qi"][0])
        f0_array.append(fit_params["f0"][0])
        s_db_array.append(tuned_lekid_network.s_db[:, from_port - 1, to_port - 1])

    # plt.figure(figsize=(8, 6))
    # plt.plot(
    #     lekid_network.f * 1e-9,
    #     lekid_network.s_db[:, from_port - 1, to_port - 1],
    #     label="Isolated Lekid Network",
    # )
    # for count, s_db in enumerate(s_db_array):
    #     plt.plot(
    #         lekid_network.f * 1e-9,
    #         s_db,
    #         label=f"Lekid Network within {z0_array[count]}$\Omega$ transmission line",
    #     )
    # plt.xlabel("Frequency (GHz)")
    # plt.ylabel(f"S{to_port}{from_port} (dB)")
    # plt.legend()
    # plt.show()

    figure, axes = plt.subplots(3, 1, figsize=(4, 10), sharex=True)
    axes[0].plot(z0_array, np.array(qc_array) * 1e-4, linestyle="none", marker="o")
    axes[0].set_ylabel("LEKID Qc (x10$^{4}$)")
    axes[1].plot(z0_array, np.array(qi_array) * 1e-9, linestyle="none", marker="o")
    axes[1].set_ylabel("LEKID Qi (x10$^{9}$)")
    axes[2].plot(z0_array, np.array(f0_array) * 1e-9, linestyle="none", marker="o")
    axes[2].set_ylabel("LEKID f0 (GHz)")
    plt.xlabel("Tuning line impedance ($\Omega$)")
    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    main()
