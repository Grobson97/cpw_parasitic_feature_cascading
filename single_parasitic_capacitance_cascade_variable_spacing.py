"""
Script to test the consistency between simulation of a simple cpw and the data produced from modelling the transmission
line in scikit-rf by extracting transmission line properties from the port parameters in a sonnet file.
"""

import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
import util.skrf_media_tools
import util.cascading_tools


def main():

    to_port = 2
    from_port = 1
    directory = r"data"
    file_name = r"50Ohm_unit_cell.s2p"
    cpw_data_file_path = os.path.join(directory, file_name)
    filter_spacing = 0.25
    top_side_filterbank = True  # Boolean to specify which half of a filter-bank to use, top side or bottom side..

    frequency_array = np.linspace(0.5e9, 3e9, num=10001)
    # Define frequency band:
    frequency_band = rf.Frequency(
        start=frequency_array[0],
        stop=frequency_array[-1],
        unit="Hz",
        npoints=frequency_array.size,
    )

    # Create rf.Media instance from cpw simulation data.
    cpw_media = util.skrf_media_tools.create_media(
        frequency_band=frequency_band,
        media_simulation_file_path=cpw_data_file_path,
        loss_tangent=0.0,
    )

    # Define f0's of filter channels:
    frequency_min = 120
    frequency_max = 180
    oversampling = 1.6
    number_of_channels = round(oversampling * 300 * np.log(frequency_max / frequency_min))
    target_f0_array = util.cascading_tools.create_target_f0_array(
        frequency_min, frequency_max, number_of_channels
    )
    side_f0_array = np.flip(target_f0_array[::2])   # Flip to make f0's start at low end and finish at high.
    if top_side_filterbank:
        side_f0_array = target_f0_array[1::2]

    full_network = util.cascading_tools.build_variable_spaced_parasitic_cascade_network(
        parasitic_capacitance=8.606968940577124e-14,
        wave_spacing_fraction=filter_spacing * 2,
        f0_array=side_f0_array,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=10e-3,
        output_line_length=10e-3,
    )

    plt.figure(figsize=(8, 6))
    plt.plot(
        full_network.f * 1e-9,
        full_network.s_db[:, from_port - 1, to_port - 1],
        linestyle="-",
    )
    plt.legend()
    plt.title(
        "Feedline throughput for CPW with %i parasitics spaced %.2f mm-wave apart"
        % (len(side_f0_array), filter_spacing)
    )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S%i%i (dB)" % (to_port, from_port))
    plt.show()


if __name__ == "__main__":
    main()
