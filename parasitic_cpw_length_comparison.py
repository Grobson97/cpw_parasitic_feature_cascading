import math
import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
import util.skrf_media_tools
import util.cascading_tools


def main():

    to_port = 2
    from_port = 1
    directory = r"data"
    file_name = r"50Ohm_unit_cell.s2p"
    cpw_data_file_path = os.path.join(directory, file_name)
    bridge_spacing = 500e-6
    bridge_parasitic_c = 1.792569790752858e-14
    line_1_length = 1e-3
    line_2_length = 5e-3
    line_3_length = 10e-3
    line_4_length = 50e-3

    frequency_array = np.linspace(0.5e9, 3e9, num=10001)
    # Define frequency band:
    frequency_band = rf.Frequency(
        start=frequency_array[0],
        stop=frequency_array[-1],
        unit="Hz",
        npoints=frequency_array.size,
    )

    # Create rf.Media instance from cpw simulation data.
    cpw_media = util.skrf_media_tools.create_media(
        frequency_band=frequency_band,
        media_simulation_file_path=cpw_data_file_path,
        loss_tangent=0.0,
    )

    # Length remaining after fitting as many parasitic unit cells in:
    line_1_remaining_length = (line_1_length - 200e-6) % bridge_spacing
    line_1_length_bridges = math.floor((line_1_length - 200e-6) / bridge_spacing)

    # Build input line of parasitic cpw with bridges.
    line_1_parasitic_network = util.cascading_tools.build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance=bridge_parasitic_c,
        fixed_parasitic_spacing=bridge_spacing,
        number_of_parasitics=line_1_length_bridges,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=line_1_remaining_length / 2,
        output_line_length=line_1_remaining_length / 2,
    )

    # Length remaining after fitting as many parasitic unit cells in:
    line_2_remaining_length = (line_2_length - 200e-6) % bridge_spacing
    line_2_length_bridges = math.floor((line_2_length - 200e-6) / bridge_spacing)

    # Build input line of parasitic cpw with bridges.
    line_2_parasitic_network = util.cascading_tools.build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance=bridge_parasitic_c,
        fixed_parasitic_spacing=bridge_spacing,
        number_of_parasitics=line_2_length_bridges,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=line_2_remaining_length / 2,
        output_line_length=line_2_remaining_length / 2,
    )

    # Length remaining after fitting as many parasitic unit cells in:
    line_3_remaining_length = (line_3_length - 200e-6) % bridge_spacing
    line_3_length_bridges = math.floor((line_3_length - 200e-6) / bridge_spacing)

    # Build input line of parasitic cpw with bridges.
    line_3_parasitic_network = util.cascading_tools.build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance=bridge_parasitic_c,
        fixed_parasitic_spacing=bridge_spacing,
        number_of_parasitics=line_3_length_bridges,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=line_3_remaining_length / 2,
        output_line_length=line_3_remaining_length / 2,
    )

    # Length remaining after fitting as many parasitic unit cells in:
    line_4_remaining_length = (line_4_length - 200e-6) % bridge_spacing
    line_4_length_bridges = math.floor((line_4_length - 200e-6) / bridge_spacing)

    # Build input line of parasitic cpw with bridges.
    line_4_parasitic_network = util.cascading_tools.build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance=bridge_parasitic_c,
        fixed_parasitic_spacing=bridge_spacing,
        number_of_parasitics=line_4_length_bridges,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=line_4_remaining_length / 2,
        output_line_length=line_4_remaining_length / 2,
    )

    plt.figure(figsize=(8, 6))
    plt.plot(
        line_1_parasitic_network.f * 1e-9,
        line_1_parasitic_network.s_db[:, from_port - 1, to_port - 1],
        linestyle="-",
        label=f"{line_1_length * 1e3} mm"
    )
    plt.plot(
        line_2_parasitic_network.f * 1e-9,
        line_2_parasitic_network.s_db[:, from_port - 1, to_port - 1],
        linestyle="-",
        label=f"{line_2_length * 1e3} mm"
    )
    plt.plot(
        line_3_parasitic_network.f * 1e-9,
        line_3_parasitic_network.s_db[:, from_port - 1, to_port - 1],
        linestyle="-",
        label=f"{line_3_length * 1e3} mm"
    )
    plt.plot(
        line_4_parasitic_network.f * 1e-9,
        line_4_parasitic_network.s_db[:, from_port - 1, to_port - 1],
        linestyle="-",
        label=f"{line_4_length * 1e3} mm"
    )
    plt.legend(title="Line Length:")
    plt.title(
        "Simulated feedline throughput for different lengths of cpw with parasitic bridges"
    )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S%i%i (dB)" % (to_port, from_port))
    plt.show()


if __name__ == "__main__":
    main()
