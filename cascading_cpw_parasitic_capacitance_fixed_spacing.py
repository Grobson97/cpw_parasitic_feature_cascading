import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
import util.skrf_media_tools
import util.cascading_tools
import math


def main():

    to_port = 2
    from_port = 1
    directory = r"data"
    file_name = r"50Ohm_unit_cell.s2p"
    cpw_data_file_path = os.path.join(directory, file_name)

    parasitic_spacing = (
        500e-6  # Total amount of transmission line between parasitic capacitance.
    )
    total_line_length = 27e-3  # Total length of transmission line.

    frequency_array = np.linspace(0.5e9, 200e9, num=10001)
    # Define frequency band:
    frequency_band = rf.Frequency(
        start=frequency_array[0],
        stop=frequency_array[-1],
        unit="Hz",
        npoints=frequency_array.size,
    )

    # Create rf.Media instance from cpw simulation data.
    cpw_media = util.skrf_media_tools.create_media(
        frequency_band=frequency_band,
        media_simulation_file_path=cpw_data_file_path,
        loss_tangent=0.0,
    )

    # Length remaining after fitting as many parasitic unit cells in:
    remaining_length = (total_line_length - 200e-6) % parasitic_spacing
    number_of_parasitics = math.floor((total_line_length - 200e-6) / parasitic_spacing)

    full_network = util.cascading_tools.build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance=8.606968940577124e-14,
        fixed_parasitic_spacing=parasitic_spacing,
        number_of_parasitics=number_of_parasitics,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=remaining_length,
        output_line_length=remaining_length,
    )

    plt.figure(figsize=(8, 6))
    plt.plot(
        full_network.f * 1e-9,
        full_network.s_db[:, from_port - 1, to_port - 1],
        linestyle="-",
    )
    plt.legend()
    plt.title(
        "Feedline throughput for %i um CPW with %i parasitics spaced %i um apart"
        % (total_line_length * 1e6, number_of_parasitics, parasitic_spacing * 1e6)
    )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S%i%i (dB)" % (to_port, from_port))
    plt.show()


if __name__ == "__main__":
    main()
