import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
import util.lekid_analysis_tools
import util.skrf_media_tools


def main():

    to_port = 1
    from_port = 1
    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\FBS_LEKIDs\hybrid_idc_lekid\300um_SiN_Qc_study"
    file_name = "hybrid_idc_lekid_auto_lengths_lossless_test_param1.s2p"

    file_path = os.path.join(directory, file_name)

    # Create renormalised port impedance network from file:
    lekid_network = util.sonnet_simulation_tools.make_renormalised_network(
        data_file_path=file_path, n_ports=2
    )

    z0_1 = 46.5
    z0_2 = 33
    tuning_line_length = 10e-3

    gamma_array = util.skrf_media_tools.calculate_gamma(
        frequency_band=lekid_network.frequency,
        effective_permittivity=complex(6.41536, 0.0),
        loss_tangent=0.0,
    )

    tuning_cpw_media_1 = rf.media.DefinedGammaZ0(
        frequency=lekid_network.frequency,
        z0=complex(z0_1, 0),
        gamma=gamma_array,
    )
    tuning_cpw_media_2 = rf.media.DefinedGammaZ0(
        frequency=lekid_network.frequency,
        z0=complex(z0_2, 0),
        gamma=gamma_array,
    )

    tuning_cpw_line_1 = tuning_cpw_media_1.line(tuning_line_length, units="m")
    tuning_cpw_line_2 = tuning_cpw_media_2.line(tuning_line_length, units="m")

    tuned_lekid_network = tuning_cpw_line_1 ** lekid_network ** tuning_cpw_line_2

    fit_params = util.lekid_analysis_tools.fit_skewed_lorentzian(
        frequency_array=lekid_network.f,
        data_array=np.abs(lekid_network.s[:, 0, 1]),
        qc_guess=5e5,
        qi_guess=1e8,
        f0_guess=None,
        normalise=False,
        plot_graph=True,
        plot_db=True,
        plot_title="Isolated LEKID network",
    )

    fit_params = util.lekid_analysis_tools.fit_skewed_lorentzian(
        frequency_array=tuned_lekid_network.f,
        data_array=np.abs(tuned_lekid_network.s[:, 0, 1]),
        qc_guess=5e5,
        qi_guess=1e8,
        f0_guess=None,
        normalise=False,
        plot_graph=True,
        plot_db=True,
        plot_title=f"LEKID network within {z0_1}$\Omega$ - {z0_2}$\Omega$ transmission line",
    )

    plt.figure(figsize=(8, 6))
    plt.plot(
        lekid_network.f * 1e-9,
        20 * np.log10(np.abs(lekid_network.s[:, 0, 1])),
        label="Isolated Lekid Network",
        linestyle="none",
        marker="o",
        markersize=3
    )
    plt.plot(
        tuned_lekid_network.f * 1e-9,
        20 * np.log10(np.abs(tuned_lekid_network.s[:, 0, 1])),
        label=f"Lekid Network within {z0_1}$\Omega$ - {z0_2}$\Omega$ transmission line",
        linestyle="none",
        marker="o",
        markersize=3
    )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel(f"S{to_port}{from_port} (dB)")
    plt.legend()
    plt.show()



if __name__ == "__main__":
    main()
