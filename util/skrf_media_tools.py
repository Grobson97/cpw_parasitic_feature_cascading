import numpy as np
import skrf as rf
import util.sonnet_simulation_tools


def calculate_gamma(
    frequency_band: rf.Frequency,
    effective_permittivity: float or complex,
    loss_tangent: float,
) -> np.ndarray:
    """Calculates the array of propagation constants and gamma, of the microstrip transmission line
    at each frequency in the frequency_band.f.

    :param frequency_band: Defines the frequency band
    :param effective_permittivity: Effective permittivity for current microstrip parameters
    :param loss_tangent: Loss tangent of the media dielectric
    :returns: Array of propagation constants and gamma, of the microstrip transmission line at each frequency in the
        frequency_band.f.
    """
    v = 2.99e8 / np.sqrt(effective_permittivity)
    alpha = (np.pi * frequency_band.f / v) * loss_tangent
    beta = 2 * np.pi * frequency_band.f / v

    gamma = alpha + 1j * beta

    return gamma


def create_media(
    frequency_band: rf.Frequency,
    media_simulation_file_path: str,
    loss_tangent: float,
) -> rf.Media:
    """Creates and returns an instance of the rf.Media.DefinedGammaZ0 class for the defined frequency_band to
    represent transmission line media from a given sonnet simulation data file for a transmission line of the given
    width and dielectric loss tangent.

    :param frequency_band: Start and stop must be within the frequency bounds of the sonnet simulation data
    :param media_simulation_file_path: File path to sonnet simulation data file of media.
    :param loss_tangent: Dielectric loss tangent

    """

    mean_port_z0 = util.sonnet_simulation_tools.get_mean_port_impedance(
        file_path=media_simulation_file_path, port_number="1"
    )
    mean_port_eeff_array = util.sonnet_simulation_tools.get_mean_port_permittivity(
        file_path=media_simulation_file_path, port_number="1"
    )

    gamma_array = calculate_gamma(
        frequency_band=frequency_band,
        effective_permittivity=complex(
            mean_port_eeff_array[0], mean_port_eeff_array[1]
        ),
        loss_tangent=loss_tangent,
    )

    microstrip_media = rf.media.DefinedGammaZ0(
        frequency=frequency_band,
        z0=complex(mean_port_z0[0], mean_port_z0[1]),
        gamma=gamma_array,
    )

    return microstrip_media
