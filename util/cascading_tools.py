import math

import skrf as rf
import numpy as np


def build_parasitic_unit_cell_network(
    frequency_band: rf.Frequency,
    cpw_media: rf.Media,
    parasitic_capacitance: float,
):

    input_length = 100e-6
    output_length = 100e-6

    # Define Circuit components
    parasitic_c1 = cpw_media.capacitor(C=parasitic_capacitance, name="Parasitic C")
    tl1 = cpw_media.line(input_length, unit="m", name="Unit cell input line")
    tl2 = cpw_media.line(output_length, unit="m", name="Unit cell output line")
    ground = rf.Circuit.Ground(frequency=frequency_band, name="GND", z0=cpw_media.z0)

    port_in = rf.Circuit.Port(frequency_band, name="Port1", z0=50.0)
    port_out = rf.Circuit.Port(frequency_band, name="Port2", z0=50.0)

    connections = [
        [(port_in, 0), (tl1, 0)],
        [(tl1, 1), (tl2, 0), (parasitic_c1, 0)],
        [(parasitic_c1, 1), (ground, 0)],
        [(tl2, 1), (port_out, 0)],
    ]

    circuit = rf.Circuit(connections)
    network = circuit.network

    return network


def build_variable_spaced_parasitic_cascade_network(
        parasitic_capacitance: float,
        wave_spacing_fraction: float,
        f0_array: np.array,
        frequency_band: rf.Frequency,
        media: rf.Media,
        input_line_length: float,
        output_line_length: float,
) -> rf.Network:
    """
    Function to build a network of a number of cascaded parasitic capacitances spaced a certain fraction of a wavelength
    apart. The number of parasitics is defined by the number of f0's in the f0 array provided when calling the function.

    :param parasitic_capacitance: Capacitance of each parasitic capacitance.
    :param wave_spacing_fraction: Fraction of the wavelength to space parasitics by. e.g. 0.5 = half wave spaced. e.g.
    if an f0 corresponds to 1mm, the space to the next parasitic will be 0.5mm.
    :param f0_array: Array of f0's defining fractional the wave spaced separation between parasitics.
    :param frequency_band: Band of frequencies.
    :param media: Media from which to build the cpw and components.
    :param input_line_length: Length in metres of transmission line before the first parasitic unit cell.
    :param output_line_length: Length in metres of transmission line after the last parasitic unit cell.
    :return Network of cascaded parasitic capacitances.
    """

    parasitic_unit_cell_network = build_parasitic_unit_cell_network(
        frequency_band=frequency_band,
        cpw_media=media,
        parasitic_capacitance=parasitic_capacitance
    )

    cascade_network = media.line(input_line_length, unit="m")

    for count, f0 in enumerate(f0_array):
        # If final f0, add output line instead of wave fraction length.
        if count == len(f0_array) - 1:
            cascade_network = (
                    cascade_network
                    ** parasitic_unit_cell_network
                    ** media.line(output_line_length, "m")
            )
        else:
            # NB: the spacing is not quite identical to the real FBS as this doesn't account for the spacing on the
            # bottom side of the filter bank mm feedline.
            spacing = calculate_parasitic_spacing(
                current_f0=f0,
                next_f0=f0,
                filter_spacing=wave_spacing_fraction
            )
            cascade_network = (
                cascade_network
                ** parasitic_unit_cell_network
                ** media.line(spacing - 200e-6, "m")
            )

    return cascade_network


def build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance: float,
        fixed_parasitic_spacing: float,
        number_of_parasitics: int,
        frequency_band: rf.Frequency,
        media: rf.Media,
        input_line_length: float,
        output_line_length: float,
) -> rf.Network:
    """
    Function to build a network of a number of cascaded parasitic capacitances with a fixed spacing apart.

    :param parasitic_capacitance: Capacitance of each parasitic capacitance.
    :param fixed_parasitic_spacing: Physical distance in metres between parasitics.
    :param number_of_parasitics: number of parasitic unit cells in the cascade.
    :param frequency_band: Band of frequencies.
    :param media: Media from which to build the cpw and components.
    :param input_line_length: Length in metres of transmission line before the first parasitic unit cell.
    :param output_line_length: Length in metres of transmission line after the last parasitic unit cell.
    :return Network of cascaded parasitic capacitances.
    """

    parasitic_unit_cell_network = build_parasitic_unit_cell_network(
        frequency_band=frequency_band,
        cpw_media=media,
        parasitic_capacitance=parasitic_capacitance
    )

    cascade_network = media.line(input_line_length, unit="m")

    for count in range(number_of_parasitics):
        # If final f0, add output line instead of wave fraction length.
        if count == number_of_parasitics - 1:
            cascade_network = (
                    cascade_network
                    ** parasitic_unit_cell_network
                    ** media.line(output_line_length, "m")
            )
        else:
            cascade_network = (
                    cascade_network
                    ** parasitic_unit_cell_network
                    ** media.line(fixed_parasitic_spacing - 200e-6, "m")
            )

    return cascade_network


def build_filter_bank_parasitic_cascade(
        bridge_and_stub_parasitic_c: float,
        bridge_parasitic_c: float,
        wave_spacing_fraction: float,
        top_side_f0_array: np.array,
        bottom_side_f0_array: np.array,
        bridge_spacing: float,
        frequency_band: rf.Frequency,
        media: rf.Media,
        input_line_length: float,
        output_line_length: float,
):

    top_side_parasitic_network = build_variable_spaced_parasitic_cascade_network(
        parasitic_capacitance=bridge_and_stub_parasitic_c,
        wave_spacing_fraction=wave_spacing_fraction,
        f0_array=top_side_f0_array,
        frequency_band=frequency_band,
        media=media,
        input_line_length=input_line_length,
        output_line_length=0.0,
    )

    # Build connecting line of parasitic cpw with bridges between sides of filterbank.
    # Length remaining after fitting as many parasitic unit cells in:
    mid_line_remaining_length = (9.3e-3 - 200e-6) % bridge_spacing
    number_of_mid_line_bridges = math.floor((9.3e-3 - 200e-6) / bridge_spacing)

    mid_line_parasitic_network = build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance=bridge_parasitic_c,
        fixed_parasitic_spacing=bridge_spacing,
        number_of_parasitics=number_of_mid_line_bridges,
        frequency_band=frequency_band,
        media=media,
        input_line_length=mid_line_remaining_length / 2,
        output_line_length=mid_line_remaining_length / 2,
    )

    # Build bottom side bridge and stub cascade network:
    bottom_side_parasitic_network = build_variable_spaced_parasitic_cascade_network(
        parasitic_capacitance=bridge_and_stub_parasitic_c,
        wave_spacing_fraction=wave_spacing_fraction,
        f0_array=bottom_side_f0_array,
        frequency_band=frequency_band,
        media=media,
        input_line_length=0.0,
        output_line_length=output_line_length,
    )

    return top_side_parasitic_network ** mid_line_parasitic_network ** bottom_side_parasitic_network



def calculate_wavelength(target_f0):
    """
    Function to return the full wavelength of a filter in the filter bank given the target f0.

    :param target_f0:
    :return:
    """
    res_width = (
            -5.69e-5 * target_f0 ** 3 + 0.033 * target_f0 ** 2 - 7.13 * target_f0 + 669
    )
    res_length = 12 + 2 * res_width
    return res_length * 2


def calculate_parasitic_spacing(current_f0: float, next_f0: float, filter_spacing: float,):
    """
    Function to calculate the spacing between parasitics along a feedline around a FBS.

    :param current_f0: f0 of current length to space filter channels by.
    :param next_f0: f0 of the next length to space filter channels by (the otherside of the fbs).
    :param filter_spacing: wavelength fraction filters are spaced by. e.g. quarter wave spaced is 0.25.
    :return:
    """

    length_1 = calculate_wavelength(current_f0)
    length_2 = calculate_wavelength(next_f0)

    return (length_1 + length_2) * filter_spacing * 1e-6


def create_target_f0_array(
    f_min: float, f_max: float, number_of_channels: int
) -> np.ndarray:
    """Returns a numpy array of log spaced target f0 values between the bounds of the frequency band.

    :param f_min: minimum frequency of the band
    :param f_max: maximum frequency of the band
    :param number_of_channels: number of channels in the filter, should be determined from resolving power and
    oversampling factor.
    :returns: A numpy array of log spaced target f0 values between the bounds of the frequency band.

    """
    target_f0_array = []
    fr = f_max
    while len(target_f0_array) < number_of_channels:
        target_f0_array.append(fr)
        fr = fr * np.exp(-(np.log(f_max) - np.log(f_min)) / (number_of_channels - 1))

    return np.array(target_f0_array)
