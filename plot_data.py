import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
from lmfit import Model
from lmfit import Parameters
from scipy import interpolate


def main():

    to_port = 1
    from_port = 1
    directory = r"data"
    models_dictionary = {"model": [], "network": []}

    for file in os.listdir(directory):
        file_name = os.fsdecode(file)
        file_path = os.path.join(directory, file_name)

        model = file_name.replace(".s2p", "").replace("_", " ")
        # Create network (normalised to 50 Ohm) from file:
        network = rf.Network(file_path)
        models_dictionary["model"].append(model)
        models_dictionary["network"].append(network)

    plt.figure(figsize=(8, 6))
    for count, model in enumerate(models_dictionary["model"]):
        line_style = "-"
        if "long" in model:
            line_style = "--"
        plt.plot(
            models_dictionary["network"][count].f * 1e-9,
            models_dictionary["network"][count].s_db[:, from_port - 1, to_port - 1],
            label=model,
            linestyle=line_style,
        )

    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S%i%i (dB)" % (from_port, to_port))
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
