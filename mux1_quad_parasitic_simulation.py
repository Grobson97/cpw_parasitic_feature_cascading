"""
Script to test the consistency between simulation of a simple cpw and the data produced from modelling the transmission
line in scikit-rf by extracting transmission line properties from the port parameters in a sonnet file.
"""
import math
import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
import util.skrf_media_tools
import util.cascading_tools


def main():

    to_port = 2
    from_port = 1
    directory = r"data"
    file_name = r"50Ohm_unit_cell.s2p"
    cpw_data_file_path = os.path.join(directory, file_name)
    filter_spacing = 0.25
    input_length = 48.5e-3
    output_length = 48.5e-3
    bridge_parasitic_c = 1.792569790752858e-14
    bridge_and_stub_parasitic_c = 8.606968940577124e-14
    bridge_spacing = 500e-6

    frequency_array = np.linspace(0.5e9, 3e9, num=10001)
    # Define frequency band:
    frequency_band = rf.Frequency(
        start=frequency_array[0],
        stop=frequency_array[-1],
        unit="Hz",
        npoints=frequency_array.size,
    )

    # Create rf.Media instance from cpw simulation data.
    cpw_media = util.skrf_media_tools.create_media(
        frequency_band=frequency_band,
        media_simulation_file_path=cpw_data_file_path,
        loss_tangent=0.0,
    )

    # Define f0's of filter channels:
    frequency_min = 120
    frequency_max = 180
    oversampling = 1.6

    # Length remaining after fitting as many parasitic unit cells in:
    input_remaining_length = (input_length - 200e-6) % bridge_spacing
    number_of_input_bridges = math.floor((input_length - 200e-6) / bridge_spacing)

    # Build input line of parasitic cpw with bridges.
    input_line_parasitic_network = util.cascading_tools.build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance=bridge_parasitic_c,
        fixed_parasitic_spacing=bridge_spacing,
        number_of_parasitics=number_of_input_bridges,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=input_remaining_length / 2,
        output_line_length=input_remaining_length / 2,
    )

    # Build top side bridge and stub cascade network:
    number_of_channels = round(oversampling * 300 * np.log(frequency_max / frequency_min))
    target_f0_array = util.cascading_tools.create_target_f0_array(
        frequency_min, frequency_max, number_of_channels
    )
    bottom_side_f0_array = np.flip(target_f0_array[::2])    # Bottom side f0's. Flip to make f0's ascend in frequency.
    top_side_f0_array = target_f0_array[1::2]   # Top side f0's

    filter_bank_cascade = util.cascading_tools.build_filter_bank_parasitic_cascade(
        bridge_and_stub_parasitic_c=bridge_and_stub_parasitic_c,
        bridge_parasitic_c=bridge_parasitic_c,
        wave_spacing_fraction=filter_spacing * 2,
        top_side_f0_array=top_side_f0_array,
        bottom_side_f0_array=bottom_side_f0_array,
        bridge_spacing=bridge_spacing,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=0.0,
        output_line_length=0.0,
    )

    # Length remaining after fitting as many parasitic unit cells in:
    inter_filter_bank_remaining_length = (3250e-6 - 200e-6) % bridge_spacing
    number_of_inter_filter_bank_bridges = math.floor((3250e-6 - 200e-6) / bridge_spacing)

    inter_filter_bank_line_network = util.cascading_tools.build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance=bridge_parasitic_c,
        fixed_parasitic_spacing=bridge_spacing,
        number_of_parasitics=number_of_inter_filter_bank_bridges,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=inter_filter_bank_remaining_length / 2,
        output_line_length=inter_filter_bank_remaining_length / 2,
    )

    # Length remaining after fitting as many parasitic unit cells in:
    output_remaining_length = (output_length - 200e-6) % bridge_spacing
    number_of_output_bridges = math.floor((output_length - 200e-6) / bridge_spacing)

    # Build input line of parasitic cpw with bridges.
    output_line_parasitic_network = util.cascading_tools.build_fixed_spaced_parasitic_cascade_network(
        parasitic_capacitance=bridge_parasitic_c,
        fixed_parasitic_spacing=bridge_spacing,
        number_of_parasitics=number_of_output_bridges,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=output_remaining_length / 2,
        output_line_length=output_remaining_length / 2,
    )

    full_network = (
        input_line_parasitic_network
        ** filter_bank_cascade
        ** inter_filter_bank_line_network
        ** filter_bank_cascade
        ** inter_filter_bank_line_network
        ** filter_bank_cascade
        ** inter_filter_bank_line_network
        ** filter_bank_cascade
        ** output_line_parasitic_network
    )

    gamma_array = util.skrf_media_tools.calculate_gamma(
        frequency_band=frequency_band,
        effective_permittivity=complex(6.41536, 0.0),
        loss_tangent=0.0,
    )

    tuning_cpw_media = rf.media.DefinedGammaZ0(
        frequency=frequency_band,
        z0=complex(200, 0),
        gamma=gamma_array,
    )

    tuning_cpw_line = tuning_cpw_media.line(10e-3, units="m")

    full_network_100Ohm = tuning_cpw_line ** full_network ** tuning_cpw_line

    # inductance_array = [5e-8, 1e-8, 1e-9, 1e-10, 1e-11]
    inductance_array = [5e-8, 1e-8]

    plt.figure(figsize=(8, 6))
    # for inductance in inductance_array:
    #     network = cpw_media.inductor(inductance) ** full_network ** cpw_media.inductor(inductance)
    #     plt.plot(
    #         network.f * 1e-9,
    #         network.s_db[:, from_port - 1, to_port - 1],
    #         linestyle="-",
    #         label=f"{inductance:.1E} H"
    #     )
    plt.plot(
        full_network_100Ohm.f * 1e-9,
        full_network_100Ohm.s_db[:, from_port - 1, to_port - 1],
        linestyle="-",
        label="200$\Omega$, 10mm port lines"
    )
    plt.plot(
        full_network.f * 1e-9,
        full_network.s_db[:, from_port - 1, to_port - 1],
        linestyle="-",
        label="No wire bonds"
    )
    plt.legend(title="Wire bond self inductance:")
    plt.title(
        "Simulated feedline throughput for a Mux1 sparse device considering all parasitics"
    )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S%i%i (dB)" % (to_port, from_port))
    plt.show()


if __name__ == "__main__":
    main()
