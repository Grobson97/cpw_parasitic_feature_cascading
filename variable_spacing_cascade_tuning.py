import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
import util.skrf_media_tools
import util.cascading_tools
import math
from scipy.optimize import minimize, rosen, rosen_der


def main():

    to_port = 2
    from_port = 1
    directory = r"data"
    file_name = r"50Ohm_unit_cell.s2p"
    cpw_data_file_path = os.path.join(directory, file_name)

    parasitic_capacitance = 8.606968940577124e-14

    frequency_array = np.linspace(0.5e9, 3.5e9, num=1001)
    # Define frequency band:
    frequency_band = rf.Frequency(
        start=frequency_array[0],
        stop=frequency_array[-1],
        unit="Hz",
        npoints=frequency_array.size,
    )

    # Create rf.Media instance from cpw simulation data.
    cpw_media = util.skrf_media_tools.create_media(
        frequency_band=frequency_band,
        media_simulation_file_path=cpw_data_file_path,
        loss_tangent=0.0,
    )

    gamma_array = util.skrf_media_tools.calculate_gamma(
        frequency_band=frequency_band,
        effective_permittivity=complex(6.41536, 0.0),
        loss_tangent=0.0,
    )

    # Define f0's of filter channels:
    frequency_min = 120
    frequency_max = 180
    oversampling = 1.6

    # Build top side bridge and stub cascade network:
    number_of_channels = round(oversampling * 300 * np.log(frequency_max / frequency_min))
    target_f0_array = util.cascading_tools.create_target_f0_array(
        frequency_min, frequency_max, number_of_channels
    )
    bottom_side_f0_array = np.flip(target_f0_array[::2])  # Bottom side f0's. Flip to make f0's ascend in frequency.
    top_side_f0_array = target_f0_array[1::2]  # Top side f0's

    filter_bank_cascade = util.cascading_tools.build_variable_spaced_parasitic_cascade_network(
        parasitic_capacitance=parasitic_capacitance,
        wave_spacing_fraction=0.25,
        f0_array=bottom_side_f0_array,
        frequency_band=frequency_band,
        media=cpw_media,
        input_line_length=0.0,
        output_line_length=0.0,
    )

    tuned_z0_array = [33, 50]

    plt.figure(figsize=(8, 6))
    for tuned_z0 in tuned_z0_array:
        tuning_cpw_media = rf.media.DefinedGammaZ0(
            frequency=frequency_band,
            z0=complex(tuned_z0, 0),
            gamma=gamma_array,
        )

        tuning_cpw_line = tuning_cpw_media.line(10e-3, units="m")

        full_network = (
            tuning_cpw_line
            ** filter_bank_cascade
            ** tuning_cpw_line
            ** filter_bank_cascade
            ** tuning_cpw_line
        )
        plt.plot(
            full_network.f * 1e-9,
            full_network.s_db[:, from_port - 1, to_port - 1],
            linestyle="-",
            label=f"{tuned_z0} $\Omega$"
        )
    plt.legend()
    plt.title(
        "Feedline throughput for a variable z0 simple CPW connecting two variable\nspacing parasitic cascades"
    )
    plt.legend(title="Tuning CPW Z0:")
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S%i%i (dB)" % (to_port, from_port))
    plt.show()


if __name__ == "__main__":
    main()
