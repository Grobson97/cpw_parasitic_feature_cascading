"""
Script to test the consistency between simulation of a simple cpw and the data produced from modelling the transmission
line in scikit-rf by extracting transmission line properties from the port parameters in a sonnet file.
"""

import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
import util.skrf_media_tools
from lmfit import Model
from lmfit import Parameters
from scipy import interpolate


def calculate_skrf_s_param(
    data_frequency_array: np.ndarray,
    to_port: int,
    from_port: int,
):

    input_length = 250.0e-6
    output_length = 250.0e-6

    # Define frequency band:
    frequency_band = rf.Frequency(
        start=data_frequency_array[0],
        stop=data_frequency_array[-1],
        unit="Hz",
        npoints=data_frequency_array.size,
    )

    # Create rf.Media instance from cpw simulation data.
    cpw_media = util.skrf_media_tools.create_media(
        frequency_band=frequency_band,
        media_simulation_file_path=r"data\50Ohm_unit_cell.s2p",
        loss_tangent=0.0,
    )

    tl1 = cpw_media.line(input_length, "m", name="Input Line")
    tl2 = cpw_media.line(output_length, "m", name="Output Line")

    port_in = rf.Circuit.Port(frequency_band, name="Port1", z0=50.0)
    port_out = rf.Circuit.Port(frequency_band, name="Port2", z0=50.0)

    connections = [
        [(port_in, 0), (tl1, 0)],
        [(tl1, 1), (tl2, 0)],
        [(tl2, 1), (port_out, 0)],
    ]

    circuit = rf.Circuit(connections)
    network = circuit.network

    return network.s_db[:, to_port - 1, from_port - 1]


def main():

    to_port = 1
    from_port = 1
    directory = r"data"
    file_name = r"50Ohm_long.s2p"
    file_path = os.path.join(directory, file_name)

    # Create network (normalised to 50 Ohm) from file:
    network = rf.Network(file_path)

    skrf_data = calculate_skrf_s_param(
        data_frequency_array=network.f, to_port=1, from_port=1
    )

    plt.figure(figsize=(8, 6))
    plt.plot(
        network.f * 1e-9,
        network.s_db[:, from_port - 1, to_port - 1],
        label="data",
        linestyle="-",
    )
    plt.plot(network.f * 1e-9, skrf_data, label="skrf data", linestyle="-")
    plt.legend()
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S%i%i (dB)" % (from_port, to_port))
    plt.show()


if __name__ == "__main__":
    main()
