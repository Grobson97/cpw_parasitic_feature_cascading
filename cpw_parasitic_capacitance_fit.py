"""
Script to test the consistency between simulation of a simple cpw and the data produced from modelling the transmission
line in scikit-rf by extracting transmission line properties from the port parameters in a sonnet file.
"""

import os
import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import util.sonnet_simulation_tools
import util.skrf_media_tools
from lmfit import Model
from lmfit import Parameters
from scipy import interpolate


def calculate_skrf_s_param(
    data_frequency_array: np.ndarray,
    capacitance: float,
    to_port: int,
    from_port: int,
):

    input_length = 100.0e-6
    output_length = 238.0e-6

    # Define frequency band:
    frequency_band = rf.Frequency(
        start=data_frequency_array[0],
        stop=data_frequency_array[-1],
        unit="Hz",
        npoints=data_frequency_array.size,
    )

    # Create rf.Media instance from cpw simulation data.
    cpw_media = util.skrf_media_tools.create_media(
        frequency_band=frequency_band,
        media_simulation_file_path=r"data\50Ohm_unit_cell_with_bridge_broken_inductor_lekid_wide.s2p",
        loss_tangent=0.0,
    )

    # Define Circuit components
    parasitic_c1 = cpw_media.capacitor(C=capacitance, name="Parasitic C")
    tl1 = cpw_media.line(input_length, "m", name="Input Line")
    tl2 = cpw_media.line(output_length, "m", name="Output Line")
    ground = rf.Circuit.Ground(frequency=frequency_band, name="GND", z0=cpw_media.z0)

    port_in = rf.Circuit.Port(frequency_band, name="Port1", z0=50.0)
    port_out = rf.Circuit.Port(frequency_band, name="Port2", z0=50.0)

    connections = [
        [(port_in, 0), (tl1, 0)],
        [(tl1, 1), (tl2, 0), (parasitic_c1, 0)],
        [(parasitic_c1, 1), (ground, 0)],
        [(tl2, 1), (port_out, 0)],
    ]

    circuit = rf.Circuit(connections)
    network = circuit.network

    return network.s_db[:, to_port - 1, from_port - 1]


def main():

    to_port = 1
    from_port = 1
    directory = r"data"
    file_name = r"50Ohm_long_with_bridge_and_stub.s2p"
    file_path = os.path.join(directory, file_name)

    # Create network (normalised to 50 Ohm) from file:
    network = rf.Network(file_path)

    # Fit to parasitic capacitance to data:
    # Define model:
    curve_model = Model(calculate_skrf_s_param)

    # Define Parameters:
    params = Parameters()
    params.add("capacitance", value=1e-15, vary=True)
    params.add("to_port", value=1, vary=False)
    params.add("from_port", value=1, vary=False)

    result = curve_model.fit(
        network.s_db[:, to_port - 1, from_port - 1],
        params,
        data_frequency_array=network.f,
    )

    ideal = result.best_fit
    capacitance = result.best_values["capacitance"]

    print(capacitance)

    plt.figure(figsize=(8, 6))
    plt.plot(
        network.f * 1e-9,
        network.s_db[:, from_port - 1, to_port - 1],
        label="data",
        linestyle="-",
    )
    plt.plot(
        network.f * 1e-9,
        ideal,
        label="skrf data with\nC=%.2E F" % capacitance,
        linestyle="--",
    )
    plt.legend()
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S%i%i (dB)" % (from_port, to_port))
    plt.show()


if __name__ == "__main__":
    main()
